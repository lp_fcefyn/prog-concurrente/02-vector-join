package pack;

public class Principal {

	public static void main (String[] args)
	{
		Hilo vec[] = new Hilo[5];
		
		for(int i = 0; i < vec.length; i++)
		{
			vec[i] = new Hilo(i+1);
			vec[i].start();
		}																							
		
		try {										// Sin esto no se sabe si el hilo principal se ejecuta antes o despues
			for(int i = 0; i < vec.length; i++)		// que los otros hilos. Nota: Si el hilo principal se ejecuta antes del resto
			{										// de los hilos, este se destruye antes que el resto, esto puede no ser deseable
				vec[i].join();			// El hilo principal espera siempre a que termine de ejecutarse el hilo i
			}										// El metodo join va siempre con un try-catch y no puede ser incluido en el for										
		}catch(Exception ex) {}						// que declara y lanza los hilos pues esto eliminaria el paralelismo (el hilo 
													// principal es el que recorre el for cque lanza los hilos, si se incluye en el
													// mismo for, se crea un hilo, se lanza, el hilo principal espera a que este hilo
													// termine y por lo tanto no lanza los demas hilos ni hace funciones del hilo principal
													// este comportamiento es secuencial).
													
		System.out.println("Soy el hilo principal");
	}
	
}