package pack;

public class Hilo extends Thread{
	private int id;
	
	public Hilo (int id)
	{
		this.id = id;
	}
	
	public void run()
	{
		System.out.println("Hola soy el hilo "+ this.getID());
	}
	
	public int getID()
	{
		return this.id;
	}
}